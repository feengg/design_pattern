﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Factory
{

    class main
    {
        //基类
        public class Operation
        {
            private double _numberA = 0;
            private double _numberB = 0;

            public double NumberA
            {
                get { return _numberA; }
                set { _numberA = value; }
            }

            public double NumberB
            {
                get { return _numberB; }
                set { _numberB = value; }
            }

            public virtual double GetResult()
            {
                double result = 0;
                return result;
            }
        }
        //加法类
        class OperationAdd : Operation
        {
            public override double GetResult()
            {
                double result = 0;
                result = NumberA + NumberB;
                return result;
            }
        }
        //减法类
        class OperationSub : Operation
        {
            public override double GetResult()
            {
                double result = 0;
                result = NumberA - NumberB;
                return result;
            }
        }
        //乘法类
        class OperationMul : Operation
        {
            public override double GetResult()
            {
                double result = 0;
                result = NumberA * NumberB;
                return result;
            }
        }
        //除法类
        class OperationDiv : Operation
        {
            public override double GetResult()
            {
                double result = 0;
                if (0 == NumberB)
                    throw new Exception("除数不能为0\n");
                result = NumberA / NumberB;
                return result;
            }
        }
        //简单工厂模式
        public class OperationFactory
        {
            public static Operation createOperate(string operate)
            {
                Operation oper = null;
                switch (operate)
                {
                    case "+":
                        oper = new OperationAdd();
                        break;
                    case "-":
                        oper = new OperationSub();
                        break;
                    case "*":
                        oper = new OperationMul();
                        break;
                    case "/":
                        oper = new OperationDiv();
                        break;
                }
                return oper;
            }
        }
        //主函数
        static void Main(string[] args)
        {
            try
            {
                Operation oper;
                Console.Write("请输入数字A：");
                string strNumberA = Console.ReadLine();
                Console.Write("请选择运算符（*、-、/、+）：");
                string strOperate = Console.ReadLine();
                Console.Write("请输入数字：");
                string strNumberB = Console.ReadLine();
                string strResult = "";

                oper = OperationFactory.createOperate(strOperate);
                oper.NumberA = Convert.ToDouble(strNumberA);
                oper.NumberB = Convert.ToDouble(strNumberB);
                strResult = Convert.ToString(oper.GetResult());

                Console.WriteLine("结果是：" + strResult);
                Console.ReadLine();
            }

            catch (Exception ex)
            {
                Console.WriteLine("您的输入有错：" + ex.Message);
                Console.ReadLine();
            }
        }
    }
}
